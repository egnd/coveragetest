tools:
	go install github.com/boumenot/gocover-cobertura@latest

test:
	mkdir -p .coverage
	go test -race -cover -covermode=atomic -coverprofile=.coverage/cover.out ./...

coverage:
	go tool cover -func .coverage/cover.out | grep "total:"
	gocover-cobertura < .coverage/cover.out > .coverage/cobertura.xml
